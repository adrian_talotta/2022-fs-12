console.log( 'Hello Node!' )

// const mathHelpers = require( './math-helpers.js' )
// // // const addiere = (a, b) => Number(a) + Number(b)

// console.log('10+20=', mathHelpers.addiere(10, 20) )
// console.log( 'isSecretNumber(555):', mathHelpers.isSecretNumber(555))
// console.log( 'isSecretNumber(22):', mathHelpers.isSecretNumber(22))

const { isSecretNumber, addiere } = require( './math-helpers.js' )

console.log( '10+20=', addiere(10, 20) )
console.log( 'isSecretNumber(555):', isSecretNumber(555))
console.log( 'isSecretNumber(22):', isSecretNumber(22))

// const primeFunction = require('./math-helpers')
// primeFunction()