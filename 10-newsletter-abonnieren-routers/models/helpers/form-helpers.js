const REGEX_MAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

/**
 * Validates a string
 * @param {string} str the string to examinate
 * @param {number} min min amount of chars
 * @param {number} max miax amount of chars
 * @returns {boolean} true, if the string is valid
 */
module.exports.validateString = (str, min=2, max=30) => str && str.length >= min && str.length <= max

/**
 * 
 * @param {*} email 
 * @returns 
 */
module.exports.validateEmail = email => ( email && REGEX_MAIL.test(email) )

/**
 * 
 * @param {*} paramsObj 
 */
module.exports.trimFormData = paramsObj => {
    for(let prop in paramsObj){
        console.log( 'Zeichenlänge Parameter ungetrimmt:', paramsObj[prop].length)
        paramsObj[prop] = paramsObj[prop].trim()
        console.log( 'Zeichenlänge Parameter getrimmt:', paramsObj[prop].length)
    }
}
