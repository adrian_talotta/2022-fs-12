const { readFile, writeFile } = require('fs/promises')
const { validateString, validateEmail, trimFormData } = require('./helpers/form-helpers')

const PATH_JSON_NEWSLETTER = process.cwd() + '/data/newsletter.json'

const validateFormData = paramsObj => {
    // API: vorname 2-30 Zeichen (white spaces am Anfang und Ende des Strings werden nicht mitgezeählt)
    // API: vorname 2-50 Zeichen (white spaces am Anfang und Ende des Strings werden nicht mitgezeählt)

    let fieldName = null
    if( ! validateString(paramsObj.vorname) )           fieldName = 'vorname'
    else if( ! validateString(paramsObj.nachname) )     fieldName = 'nachname'
    else if( ! validateEmail(paramsObj.email) )         fieldName = 'email'

    if( fieldName ) {
        return getErrorNoticationObject(`Die Angabe ${fieldName} enthält keinen gültigen Wert.`, fieldName)
    } else {
        return null
    }
}

const saveSubscription = formData => {
    return new Promise( async (resolve, reject) => {
        try {
            const fileData = await readFile(PATH_JSON_NEWSLETTER)
            const jsonData = JSON.parse(fileData)
            const subscriber = { vorname, nachname, email } = formData
            jsonData.subscribers.push(subscriber)
            await writeFile(PATH_JSON_NEWSLETTER, JSON.stringify(jsonData))
            resolve()
        } catch (err) {
            console.log('Error', err.name, err.message, err.code)
            reject(err)
            // throw err
        }
    } )
}

const getErrorNoticationObject = (msg, fieldName='') => {
    return {
        fieldName: fieldName,
        message: msg
    } 
}

module.exports.subscribe = subscriber => {
    return new Promise( async (resolve, reject) => {
        trimFormData( subscriber )
        const err = validateFormData( subscriber )

        if( err ){
            reject( err )
        } else {
            try{
                await saveSubscription( subscriber )
                resolve( null )
            } catch(err){
                reject( getErrorNoticationObject(err.msg) )
            }
        }
    })
}
