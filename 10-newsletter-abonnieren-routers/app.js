// Server Initialisierung
const express = require('express')
const routerNewsletter = require( './routers/newsletter' )

const PORT = process.env.PORT || 5000

const app = express()
app.listen(PORT, () => console.log('Server läuft auf Port', PORT))

app.set( 'view engine', 'ejs' )

/////////////////////////////
// Middleware Funktion
/////////////////////////////

app.use( express.static('./www') )
app.use( express.urlencoded( { extended: false }) )

// Middleware-Funktion für eingehenden Requests, die mit dem Pfad '/newsletter' BEGINNEN.
// Innerhalb des Routers wird dieser Anfangsteil des Pfads nicht weiter berücksichtigt.
// D.h.: Der Pfad '/newsletter/test' wird innerhalb des Routers als Pfad '/test' weiterverarbeietet oder '/newsletter' führt dem als Pfad '/' übergeben.
app.use('/newsletter', routerNewsletter)



// Endpoints / Routes
app.get( '/', (req, res) => res.render( 'index', { errorFieldName: '', errorMsg: '', formData: null} ))



console.log('project root:', process.cwd() )