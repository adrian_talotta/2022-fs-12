const { Router } = require('express')
const { subscribeAction, testAction } = require( '../controllers/newsletter' )

const router = new Router()

router.post('/', subscribeAction)
router.get('/test', testAction)

module.exports = router