const { subscribe } = require('../models/newsletter')

module.exports.subscribeAction = async (req, res) => {
    try{
        const err = await subscribe( { ...req.body } )
        res.render( 'success', {
            vorname: req.body.vorname,
            nachname: req.body.nachname
        } )
    } catch(err) {
        res.render( 'index', {
            errorMsg: err.message,
            errorFieldName: err.fieldName, 
            formData: req.body
        })
    }
}

module.exports.testAction = (req, res) => {
    res.send('Test succeeeded')
}