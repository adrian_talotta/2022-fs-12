# NodeJS Basis

### Releases

- alle sechs Monate erscheint ein neuer NodeJS Major Release 

- ungerade Release-Versionen (15.x, 17.x, 19.x,...) stellen Zwischenversionen ohne Langzeit-Support dar und sind für die Verwendung im Produktivsystem ungeeignet

- Release-Status

    - Current  
    Alle Major Versionen bleiben nach ihrem Release für sechs Monate im Current-Status. Dies gibt Entwicklern von Libraries die Zeit die neue Version entsprechend zu unterstützen. Möglicherweise können in dieser Zeit Probleme beim Installieren und Verwenden bestimmter Module auftreten.

    - LTS (Active und Maintanance LTS)  
    Sechs Monate nach ihrem Release gehen gerade Versionen (16.x, 18.x, 20.x,...) in den LTS-Status (long-term support) und sind damit für den Einsatz im Produktivsystem geeignet. Kritische Bugs werden in den kommenden 30 Monate gepatched.

    - Unsupported  
    Ungerade Versionen gehen sechs Monate nach ihrem Release direkt in den Unsupported-Status. Gleiches gilt für gerade Versionen nach Ablauf ihres 30-monatigen LTS-Status. Kritische Bugs werden nicht weiter gefixt. 

    - Roadmap: https://nodejs.org/de/about/releases/


### Installation

- Gehe zur Download-Seite: https://nodejs.org/en/download/
- Downloade den Installer (LTS Version) und führe ihn aus 
- Neustart VS Code
- Teste die Installation, wie in [Node CLI](#node-cli) beschrieben
- Mit der Standard-Installation von NodeJS wird die Software NPM (Node Package Manager) automatisch mitinstalliert
- Weiterführende Infos zur Installation: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

### Was ist NodeJS?
- NodeJS ist eine plattformübergreifende JavaScript-Laufzeitumgebung (Windows, Mac, Linux und Mini Computer wie Rasperry)
- NodeJS ist Open Source
- NodeJS wurde für die Implementierung von Webservern entwickelt, lässt sich aber auch für die Erstellung einer Vielzahl anderer Programme als Laufzeitumgebung nutzen
- sämtliche Basisfunktionalitäten eines Webservers werden über bereits mitgelieferte JS Module unterstützt
- eine Vielzahl weiterer JS Module (ca. 800.000) lassen sich über die  Software NPM (Node Package Manager) installieren und nutzen
- NodeJS lässt sich als Kommandozeilenprogramm (CLI/Command Line Interface) ausführen

### Anwendungsszenarien
- Web Server / Web Apps
- Mobile Apps
- Desktop Apps
- Automatisierte Prozesse
- IoT

### JavaScript im Browser 
- der JS-Engine ist als JS-Laufzeitumgebung (Interpreter) im Browser implementiert
    - Chrome & Edge: V8
    - Firefox: SpiderMonkey
    - Safari: JavaScriptCore bzw. SquirrelFish
- diverse Browser APIs sind verfügbar (GeoLocation, Web Storage, window, document, ...)
- JS läuft in der Security Sandbox des Browsers, es ist z.B. kein direkter Zugriff auf das Betriebssystem möglich (Lesen oder Schreiben von Dateien)

### JavaScript in NodeJS
- NodeJS verwendet V8, den JS-Engine von Chrome, in der jeweils aktuellen Version
- die Sicherheitsbeschränkungen des Browsers entfallen (Security Sandbox, Same-Origin-Policy/CORS)
- keine Browser APIs verfügbar (wie z.B. der HTML DOM bzw. window)
- sehr ressourcensparende Architektur durch:
    - nicht-blockierende Ein- und Ausgabe (Auslagerung von Lese- und Schreibaufgaben an das Betriebssystem)
    - Ausführung der Anwendung als Single Thread
- sehr hohe Performance
- große Anzahl gleichzeitig bestehender Netzwerkverbindungen möglich
  

### Node CLI (Command Line Interface)
- Terminal im Stammverzeichnis des Projekts öffnen (in VS Code mit rechter Maustaste auf das Verzeichnis klicken und 'In integrierten Terminal öffnen' ausführen)
- im Terminal lässt sich das Node CLI über ``` node ``` aufrufen
- die Terminal-Eingabe ``` node -v ``` gibt die installierte NodeJS Version aus
- die Terminal-Eingabe ``` node dateiname.js ``` führt die angegebene Datei als Programm aus

### NodeJS Debugging via Chrome Developer Tools

#### In Chrome

- Eingabe "chrome://inspect" in der Browser Adressleiste
- Klick auf den "Configure"-Button
- Überprüfung, ob der Host/Port '127.0.0.1:9229' in der Liste aufscheint
- Klick auf "Open dedicated DevTools for Node"

#### In VS Code

- Projekt via 'node --inspect filename.js' öffnen
- Ready!

Weiteres siehe:  
https://nodejs.org/en/docs/guides/debugging-getting-started/