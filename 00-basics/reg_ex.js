   // Eine Regular Expression (RegExp-Óbjekt) besteht aus einem 'pattern' und einem optionalen 'modifier'.

    // Zum Erzeugen einer RegExp steht einerseits der Aufruf einer Konstruktorfunktion zur Verfügung: new RegExp(pattern, modifier)
    // const re1 = new RegExp(/hello/, 'gi');

    // Meist werden Regular Expressions über die Literalschreibweise gebildet: /pattern/modifier
    // Mögliche Modifier: i=case insensitive, g=global, m=multiline

    const re1 = /hello/gi
    const baseStr = 'Hello, hello world!'

    console.log('Basis Syntax:', baseStr.replace(re1, 'hi'))

    ////////////////////////
    // Quantifiers
    ////////////////////////

    // Quantifier *
    // das vorhergehende Zeichen darf beliebig oft vorkommen (0 - n Mal)
    console.log('Quantifier *:', 'ABC AC ADC Abbc aBBBBBBC'.replace(/AB*C/ig, '$'))

    // Quantifier +
    // das vorhergehende Zeichen muss einmal, darf aber beliebig oft vorkommen (1 - n Mal)
    console.log('Quantifier +:', 'ABC AC ADC Abbc aBBBBBBC'.replace(/AB+C/ig, '$'))

    // Quantifier {min,max}
    // 'min' legt die mindeste Anzahl an Vorkommnissen fest, 'max' die maximale Anzahl (min - max Mal)
    console.log('Quantifier {min,max}:', 'ABC AC ADC Abbc aBBBBBBC'.replace(/AB{2,10}C/ig, '$'))

    // Quantifier ?
    // das vorhergehende Zeichen darf höchstens einmal oder gar nicht vorkommen (0 - 1 Mal)
    console.log('Quantifier ?:', 'ABC AC ADC Abbc aBBBBBBC'.replace(/AB?C/ig, '$'))


    ////////////////////////
    // Metacharacters
    ////////////////////////

    // Metacharacter .
    // Die Zeichenklasse (Metacharacter) '.' steht für ein beliebiges Zeichen
    console.log('Metacharacter .:', 'ABC AC ADC Abbc aBBBBBBC'.replace(/A.C/ig, '$'))

    // Metacharacter . und Quantifier * (Alle Substrings, die mit A/a beginnen und mit C/c enden)
    console.log('Metacharacter . und Quantifier * :', 'ABC AC ADC Abbc aBBBBBBC'.replace(/A.*C/ig, '$'))
    // Alles wird als ein Suchstring ersetzt, da der gesamte String mit 'A' beginnt und mit 'C' endet. 
    // Standardmäßig wird im Zweifelsfall immer nach dem längsten Teilstring gesucht (=greediness/Gierigkeit)

    // Quantifier '*?', '+?' und '??' entsprechen '*', '+' und '?' als 'nicht-gierige' Varianten
    console.log('Metacharacter . und Quantifier *?:', 'ABC AC ADC Abbc aBBBBBBC'.replace(/A.*?C/ig, '$'))
    // Alle 'Worte' werden einzeln ersetzt 

    // Metacharacter [abd]
    // Eckige Klammern legen den erlaubten Zeichenbereich fest
    console.log('Metacharacter [bd]:', 'ABC AC ADC Abbc aBBBBBBC'.replace(/A[bd]C/ig, '$'))

    console.log('Metacharacter [acb ] und Quantifier +:', 'ABC AC ADC Abbc aBBBBBBC'.replace(/[acb ]+/ig, '$'))
    // Ausgabe $D$ (D ist nicht im Zeichenraum enthalten, die Suche erfolgt standardmäßig 'gierig')

    console.log('Metacharacter [acb ] und Quantifier +?:', 'ABC AC ADC Abbc aBBBBBBC'.replace(/[acb ]+?/ig, '$'))
    // Alle Zeichen werden als einzelne Muster erkannt, da der Quantifier (+?) nicht gierig ist

    // Metacharacter [a-z] mit Zeichenraum
    console.log('Metacharacter [a-z] und Quantifier :', 'ABC AC ADC AbÖbc aBBBC'.replace(/[a-z]{3,5}/ig, '$'))
    //'AC' bleibt erhalten, da weniger als 3 Zeichen, 'AC' bleibt erhalten, da 'Ö' nicht im Zeichenraum enthalten
    // Leerzeichen dient auch hier als Trennzeichen, da nicht im Zeichenraum enthalten

    // [^abc]
    // Eckige Klammern mit einem '^' als erstes Zeichen legen den NICHT erlaubten Zeichenbereich fest
    console.log('Zeichenbereich "[abc ]": ', 'ABC AC AXXc ABBBC'.replace(/A[^B]*C/gi, '$') );   
    // Nur 'AC' und 'AXXC' werden ersetzt. (Zwischen A und C ist weder B, noch ein Leerzeichen erlaubt)

    const url = 'http://public.domain.at/irgendwas'
    const urlParts = /[^.]+/.exec(url)
    console.log('urlParts');
    console.dir(urlParts);
    console.log('Subdomain:', urlParts[0].substring(7));


    // /abc|def/
    // Der pipe character (|) verbindet mehrere alternative PATTERNS
    const url2 = 'https://public.domain.at/irgendwas'
    console.log('protocol http:', /https|http/.exec(url)[0]);
    console.log('protocol http2:', /https|http/.exec(url2)[0]);

    // Aufgabe: überprüfe ob ein String den Zahlenraum 1990-2020 abbildet
    console.log('2021 in range:', /199[0-9]|20[01][0-9]|2020/.test('2021'))
    console.log('2020 in range:', /199[0-9]|20[01][0-9]|2020/.test('2020'))
    console.log('1989 in range:', /199[0-9]|20[01][0-9]|2020/.test('1989'))
    console.log('1999 in range:', /199[0-9]|20[01][0-9]|2020/.test('1999'))


    /////////////////////////////////
    // Assertions (Zusicherungen)
    /////////////////////////////////

    // ^ definiert (außerhalb von eckigen Klammern) das erste Zeichen des untersuchten Strings
    console.log('Assertion ^B: ', 'ABC AC AXXc ABBBC'.replace(/^B.*/i, '$')); 
    // Nicht wird ersetzt, da das erste Zeichen A und nicht B ist

    console.log('Assertion ^A: ', 'ABC AC AXXc ABBBC'.replace(/^A.*/i, '$')); 
    // Alles wird ersetzt, da das erste Zeichen ein A ist
    
    // $ definiert das letzte Zeichen des untersuchten Strings
    console.log('Assertion $: ', 'ABC AC AXXc ABBBC'.replace(/.*B$/i, '$'));
    // Nicht wird ersetzt, da das letzte Zeichen C und nicht B ist

    console.log('Assertion $: ', 'ABC AC AXXc ABBBC'.replace(/.*C$/i, '$')); 
    // Alles wird ersetzt, da das letzte Zeichen C ist

    
    console.log('Assertion ^ und $: via RegExp.prototype.test(): ', /^[A-Z]+C$/gi.test('ABC AC Abbc AÖC') ); // false

    // Ö und Leerzeichen sind im Zeichenbereich miteingeschlossen
    console.log('Assertion ^ und $: via RegExp.prototype.test(): ', /^[A-ZÖ ]+C$/gi.test('ABC AC Abbc AÖC') ); // true
    
    // Ö und \s sind im Zeichenbereich miteingeschlossen
    // \s findet alle Unicode Leerzeichen (white space), d.h. Leerzeichen, Umbrüche, Tabulatoren und typographische Sonderleerzeichen.
    console.log('Assertion ^ und $: via RegExp.prototype.test(): ', /^[A-ZÖ\s]+C$/gi.test('ABC AC Abbc AÖC') ); // true
    







