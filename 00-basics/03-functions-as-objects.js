
// Funktion sind in JS Objekte (sie leiten sich also von der Object-Klasse ab).
// Insofern verfügen Funktionen auch über Eigenschaften und Methoden 
// (siehe: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)

function test (){
    console.log('Hello Test!')
}

test.apply(null)
console.log( 'Parameteranzahl function test:', test.length )
test()

// Auch lassen sich Funktionen, wie auch allen anderen dynamischen Objekten in JS, 
// "on-the-fly" neue Eigenschaften und Methoden zuweisen
test.neueMethode = () => console.log( 'Hallo neue Methode' )
test.neueMethode()
