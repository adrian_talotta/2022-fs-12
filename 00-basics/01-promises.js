///////////////////////////////////////////////////
//  Asynchrone Abläufe und Callback Funktionen
///////////////////////////////////////////////////

// const warteAufWas = callback => {
//     setTimeout(
//         () => {
//             let success = true
//             if(success) callback( 'Ich tu schon was' )
//             else        callback( new Error('Leider etwas schief gegangen') )
//         },
//         3000 // in ms
//     )
// }

// console.log( 'Programmstart' ) 

// const tuWas = () => {
//     warteAufWas( msg => { 
//         console.log( msg )
//         warteAufWas( msg => { 
//             console.log( msg )
//             warteAufWas( msg => { 
//                 console.log( msg )
//                 warteAufWas( msg => { 
//                     console.log( msg )
//                 } )
//             } )
//         } )
//     } )
//     console.log( 'Das Programm geht weiter...' ) 
// }

// tuWas()

///////////////////////////////////////////////////
//  Asynchrone Abläufe und Promises
///////////////////////////////////////////////////

const warteAufWas = () => {
    const versprechenAufRueckmeldung = new Promise(
        // Die Exekutor-Funktion eines Promises verfügt über zwei Parameter (resolve, reject).
        // 'resolve' ist dabei eine Funktion, die im Erfolgsfall aufgerufen wird,
        // 'reject' eine zweite Funktion, die Fehlerfall aufgerufen wird. 
        (resolve, reject) => {
            setTimeout(
                () => {
                    let success = true
                    if(success)     resolve( 'Ich tu schon was' )
                    else            reject( new Error('Leider etwas schief gegangen') )
                },
                3000 // in ms
            )
        }
    )

    return versprechenAufRueckmeldung
}

console.log( 'Programmstart' ) 

// Abarbeiten von Promises mit async/await
// async und await lassen sich NUR in Kombination anwenden

// const tuWas = async () => {
//     try {
//         let msg = await warteAufWas()
//         console.log( msg )
//         msg = await warteAufWas()
//         console.log( msg )
//         msg = await warteAufWas()
//         console.log( msg )
//         msg = await warteAufWas()
//         console.log( msg )
//     } catch( err ) {
//         console.error( err.message )
//     }
// }


// Abarbeiten von Promises mit den Methoden .then() und .catch()

const tuWas = () => {
    warteAufWas()
        .then( msg => {
            console.log( msg )
            return warteAufWas()
        } )
        .then( msg => {
            console.log( msg )
            return warteAufWas()
        } )
        .then( msg => {
            console.log( msg )
            return warteAufWas()
        } )
        .then( msg => {
            console.log( msg )
            return warteAufWas()
        } )
        .catch( err => console.error( err.message ))
}


tuWas()
console.log( 'Das Programm geht weiter...' ) 

// Weiterführende Infos:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise