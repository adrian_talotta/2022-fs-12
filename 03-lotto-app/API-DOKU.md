# API Doku

## HTTP Request

### URL (Route): /getnumbers?numtipps=<number 1-10>    
( absolute URL: 127.0.0.1:5000/getnumbers=<number> )  

### HTTP-Methode: GET

___

## HTTP Response

success: 
{
    "tipps": [
        [6, 14, 22, 35, 39, 41],
        [16, 18, 29, 31, 38, 45],
        [1, 11, 21, 29, 32, 40]
    ],
    "error": ""
}

error:
{
    "tipps": [],
    "error": "*Error message*"
}