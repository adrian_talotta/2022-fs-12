// const { getRandomInt } = require( './math-helpers' )

const rn = require( 'random-number' )

const getQuicktipps = ( numTipps ) => {

    if( ! Number.isInteger( numTipps )  || numTipps < 1 || numTipps > 10 ){
          console.error( 'qucktipps.js/getQuicktipps: Invalid parameter, numTipps:', numTipps)
          return null     
    }

    const tipps = []

    for( let i = 1; i <= numTipps; i++ ){
        tipps.push( generateTipp() )
    }

    return tipps
}

/**
 * Generates a single quicktipp (6 * 1-45)
 * @returns an array with six numbers
 */
const generateTipp = () => {
    const tipp = []

    while( tipp.length < 6 ){
        // const int = getRandomInt(1, 45)
        const int = rn( {
            min: 1,
            max: 45,
            interger: true
        } )
        if( !tipp.includes(int) ) tipp.push(int)
    }

    return tipp.sort()
}

// const generateTipp = () => {
//     const tipp = []

//     for(let i = 0; i < 6; i++){
//         const int = getRandomInt(1, 45)
//         if( tipp.includes(int) )  i--
//         else                      tipp.push(int)
//     }
// }


module.exports = getQuicktipps

//module.exports.getQuicktipps = getQuicktipps