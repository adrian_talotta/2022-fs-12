/**
 * Provides a random integer within a given range
 * @param {integer} min the lowest possible
 * @param {integer} max the highest possible
 * @returns a random integer
 */
 const getRandomInt = (min=1, max=45) => {
    if( 
        Number.isInteger(min) === false || 
        Number.isInteger(max) === false ||
        max < min
    ){
        // Validierung gescheitert
        console.error( 'qucktipps.js/getRandomInt: Invalid parameter, min:', min, ', max:', max )
        return null
    }
    return Math.floor(Math.random() * (max-min+1)) + min
}

module.exports.getRandomInt = getRandomInt