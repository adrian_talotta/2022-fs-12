console.log( 'main.js geladen' );


window.onload = () => {

    const MAX_NUM_TIPPS = 10;
    const URL_WEB_API = '/getnumbers?numtipps='

    // Initialisierung
    const btnTipps = document.querySelector('#view_input button');
    const btnBack = document.querySelector('#view_result button');
    const select = document.querySelector('#selectAmount');
    const tableParent = document.querySelector('#tipps');

    // Befülle <select>-Element
    let html = '';
    for(let i = 1; i <= MAX_NUM_TIPPS; i++){
        html += `<option value="${i}">${i}</option>`;
    }
    select.innerHTML = html;

    // Programmlogik
    
    const getNumQuicktipps = () => select.value; // entspricht: () => { return select.value; }

    const getTipps = () => {
        axios.get( URL_WEB_API + getNumQuicktipps() )
            .then( jsonObj => generateView( jsonObj.data.tipps ) )
            .catch( err => console.error( 'HTTP Fehler beim AJAX Request:', err.name, err.message ) )
    }

    const generateView = tipps => {  
        
        console.log( tipps );

        clearTippsView();                       // Reset
        
        const numTipps = getNumQuicktipps();

        for(let i = 0; i < numTipps; i++){      // je Quicktipp

            const tipp = tipps[i];              // Der zugehörige Quicktipp wird aus der Liste aller Quicktipps bezogen
            const table = document.createElement( 'table' );
            let tr;

            for (let j = 1; j <= 45; j++){
                
                if(j % 6 === 1) {
                    tr = document.createElement( 'tr' );
                    table.appendChild(tr);
                }

                const td = document.createElement( 'td' );
                td.innerText = j;
                if( tipp.includes(j) ) td.classList.add( 'selected' );

                tr.appendChild(td);
            }

            tableParent.appendChild(table);
        }

        changeView();
    }

    const clearTippsView = () => {
        tableParent.innerHTML = '';
    }

    // UI Logik
    const changeView = () => {
        const viewInput = document.querySelector('#view_input');
        const viewResult = document.querySelector('#view_result');
        viewInput.classList.toggle('hidden');
        viewResult.classList.toggle('hidden');
    }

    // Handler & Listener
    btnTipps.onclick = () => {
        getTipps();
    }

    btnBack.onclick = () => changeView();

}; // Ende window.onload