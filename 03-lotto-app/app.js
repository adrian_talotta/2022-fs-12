// Importiert das gesamte Standardmodul 'http'
// const http = require( 'http' )

// Importieren der benötigten Methode (über ECMA6 Object Destructuring)
const { createServer } = require( 'http' )
const { readFile } = require('fs/promises')
const getQuickTipps = require( './my_modules/quicktipps')

const PORT = process.env.PORT || 5000

createServer( (req, res) => {
    console.log( 'HTTP Request, url:', req.url )
    console.log('Host-Adresse des Requests:', req.headers.host )
    const url = new URL( req.url, 'http://' + req.headers.host )

    // Routing
    if(
        url.pathname === "/getnumbers" && 
        req.method === 'GET'
    ){
        // Route: GET /getnumbers
        let error = ''
        let tipps = []

        if(
            Number(url.searchParams.get( 'numtipps' )) < 1 ||
            Number(url.searchParams.get( 'numtipps' )) > 10
        ){
            error = 'Der Parameter numtipps wurde nicht gesetzt oder is out range.'
        } else {
            tipps = getQuickTipps( Number( url.searchParams.get('numtipps') ) )
            // console.log( tipps )
        }
                
        const data = {
            tipps: tipps,
            error: error
        }

        res
            .writeHead( 200, 'OK', { 
                'Content-Type': 'application/json',
                'Access-Control-Allow-Methods': 'GET, POST',
                'Access-Control-Allow-Origin': '*'
             })
            .end( JSON.stringify(data) ) 
    } else {
        // res.writeHead( 404 , 'File Not Found' )
        // res.end()

        // TODO: try to resolve paths of a GET Request within 'public' directory
        if(req.method === 'GET'){
            console.log( 'Stammverzeichnis von app.js:', __dirname )

            if( url.pathname === '/' ) url.pathname = '/index.html'
            const path = `${ __dirname }/public${ url.pathname }`

            console.log( 'Aufgelöster Pfad des GET-Requests', path )

            readFile( path )
                .then( fileData => {
                    res.end( fileData )
                } )
                .catch( err => {
                    res
                    .writeHead( 404 , 'File Not Found' )
                    .end()
                })
        } else {
            // Andere HTTP-Methode als GET...
            res
                .writeHead( 404 , 'File Not Found' )
                .end()
        }

    }

}).listen( PORT, () => console.log( 'Server runs on port', PORT ) ) 

// Test code

// console.log( getQuickTipps(6) )

// const obj = {
//     data: [ 1, 2, 3 ]
// }

// const json = JSON.parse(obj)

// const obj2 = JSON.parse( json )