const { watch } = require( 'fs' )
const { fork } = require( 'child_process' )

const FILE_NAME = './child-processes/app.js'

// Starten des Subprozesses (Child Process Modul, fork())
let childProcess = fork( FILE_NAME )

// Beobachten der Datei des Subprozesses (File System Modul, watch())
watch( FILE_NAME, (eventType, filename) => {
    console.log( 'Datei wurde geändert.' )
    console.log( 'Art der Dateiänderung:', eventType )
    console.log( 'Geänderte Datei:', filename )
    childProcess.kill()
    // childProcess = fork( FILE_NAME )
})

childProcess.on('close', (code, signal) => {
    console.log(`child process terminated due to receipt of signal ${signal}`)
    childProcess = fork( FILE_NAME )
})