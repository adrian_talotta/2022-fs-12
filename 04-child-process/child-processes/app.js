const http = require( 'http' )

const server = http.createServer( (req, res) => {
    console.log( 'HTTP Request eingelangt, Methode:', req.method, ', URL:', req.url )
    res.end( 'Hello world! How are you?' )
})

server.listen( 5002, () => console.log( 'Server gestartet auf Port 5002') )