// Imports
const express = require( 'express' )

//////////////////////////////
// Initialisierung
//////////////////////////////

const PORT = process.env.PORT || 5000
const TEMPLATE_ENGINES = [ 
    { name: 'EJS', url: 'https://ejs.co/' },
    { name: 'PUG', url: 'https://pugjs.org/' },
    { name: 'Handlebars', url: 'https://handlebarsjs.com/' }
]

const app = express()
app.listen( PORT, () => console.log( 'Server läuft auf Port', PORT ) )

// EJS wird als Template Engine für Express festgelegt
// Application.set() legt Settings zur Server-Applikation fest
// Siehe: https://expressjs.com/en/4x/api.html#app.set
// Das Standard-Verzeichnis für den Zugriff auf HTML-Templates ist '/views', 
// die Datei-Endung von EJS-Templates is '.ejs
app.set( 'view engine', 'ejs' )


//////////////////////////////
// Middleware & Routing
//////////////////////////////

app.get('/', (req, res) => {
    res.render( 'index',
    {
        nachricht: 'Super, das EJS-Template funktioniert',
        engines: TEMPLATE_ENGINES
    })
})

app.get('/login', (req, res) => res.render( 'login' ) )

// let testObj = {
//     nachricht: 'Super, das EJS-Template funktioniert',
//     engines: TEMPLATE_ENGINES
// }

// for (key in testObj){
//     console.log('testObj enthält die Eigenschaft', key, 'mit dem Wert', testObj[key])
// }