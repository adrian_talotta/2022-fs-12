// Server Initialisierung
const express = require( 'express' )
const { readFile, writeFile } = require('fs/promises')
const { validateFormData, trimFormData } = require('./helpers/form-data')

const PORT = process.env.PORT || 5000
const PATH_JSON_NEWSLETTER =  __dirname + '/data/newsletter.json' 

const app = express()
app.listen( PORT, () => console.log( 'Server läuft auf Port', PORT ) )

/////////////////////////////
// Middleware Funktion
/////////////////////////////

// express.static(): 
// Middleware-Funktion zum Übertragen statischer Dateien aus einem Verzeichnis.
// Es wird versucht eingehende Routes (Pfade) von GET-Requests mit diesem Ordner als Stammverzeichnis (root folder) aufzulösen. D.h. aus dem Request-Pfad '/index.html' wird der Pfad 'www/index.html' gebildet.
// Falls die statische Datei gefunden wird, wird sie automatisch im Response an den Client geschickt.
// Andernfalls wird die nächste Middleware-Funktion aufgerufen.
app.use( express.static( './www' ) )

// express.urlencoded():
// Middleware-Funktion zum Parsen von Formular-Daten aus dem Request Body.
// Die url-kodierten Parameter eines Formulars werden zu Eigenschaften des Request Body-Objekts konvertiert.
// Findet u.a. Anwendung bei POST-Requests von Formularen (<form method="POST">).
// Nachträglicher Zugriff: req.body.parametername
app.use( express.urlencoded( { extended: false } ) )


// Endpoints / Routes

app.post( '/newsletter', (req, res) => {
    console.log( 'Neue Newsletter Anmeldung ')
    console.log( req.body ) 
    trimFormData( req.body )
    validateFormData( req.body )

    readFile( PATH_JSON_NEWSLETTER + '...' )
        .then ( fileData => {
            const jsonData = JSON.parse( fileData )
            const subscriber = { vorname, nachname, email } = req.body
            jsonData.subscribers.push( subscriber )
            return writeFile( PATH_JSON_NEWSLETTER,  JSON.stringify( jsonData ) )
        })
        .then( () => res.send( 'Newsletter-Anmeldung war erfolgreich XXXXXXXXXX' ) )
        .catch( err => {
            res.send( 'Eine Newsletter Anmeldung ist momentan leider nicht möglich. Aufgetretener Fehler') 
        })
})





