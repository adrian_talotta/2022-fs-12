// Server Initialisierung
const express = require( 'express' )
const { readFile, writeFile } = require('fs/promises')

const PORT = process.env.PORT || 5000
const PATH_JSON_NEWSLETTER =  __dirname + '/data/newsletter.json' 

const app = express()
app.listen( PORT, () => console.log( 'Server läuft auf Port', PORT ) )

/////////////////////////////
// Middleware Funktion
/////////////////////////////

// express.static(): 
// Middleware-Funktion zum Übertragen statischer Dateien aus einem Verzeichnis.
// Es wird versucht eingehende Routes (Pfade) von GET-Requests mit diesem Ordner als Stammverzeichnis (root folder) aufzulösen. D.h. aus dem Request-Pfad '/index.html' wird der Pfad 'www/index.html' gebildet.
// Falls die statische Datei gefunden wird, wird sie automatisch im Response an den Client geschickt.
// Andernfalls wird die nächste Middleware-Funktion aufgerufen.
app.use( express.static( './www' ) )

// express.urlencoded():
// Middleware-Funktion zum Parsen von Formular-Daten aus dem Request Body.
// Die url-kodierten Parameter eines Formulars werden zu Eigenschaften des Request Body-Objekts konvertiert.
// Findet u.a. Anwendung bei POST-Requests von Formularen (<form method="POST">).
// Nachträglicher Zugriff: req.body.parametername
app.use( express.urlencoded( { extended: false } ) )


// Endpoints / Routes
app.post( '/newsletter', (req, res) => {
    console.log( req.body )
    // TODO: Formulardaten validieren

    readFile( PATH_JSON_NEWSLETTER )
    // readFile( 'xy.json' )
        .then ( fileData => {
            const jsonData = JSON.parse( fileData )
            console.log( 'JSON-Daten vor der Editierung:', jsonData)
            
            // const subscriber = {
            //     vorname: req.body.vorname,
            //     nachname: req.body.nachname,
            //     email: req.body.email
            // }

            // const { vorname, nachname, email } = req.body
            // const subscriber = { vorname, nachname, email }

            const subscriber = { vorname, nachname, email } = req.body

            jsonData.subscribers.push( subscriber )
            return writeFile( PATH_JSON_NEWSLETTER,  JSON.stringify( jsonData ) )
        })
        .then( () => res.send( 'Newsletter-Anmeldung war erfolgreich' ) )
        .catch( err => {
            // console.log( 'Fehlertyp:', err.name, err.message)
            let errorMsg = 'Ubekannnter Fehler'
            if(err.name === 'TypeError') errorMsg = 'Es ist ein TypeError aufgetreten'
            else if(err.name === 'ReferenceError') errorMsg = 'Es ist ein ReferenceError aufgetreten'
            else if(err.name === 'Error' && err.message.includes('ENOENT')) errorMsg = 'Es ist ein ENOENET aufgetreten'
            res.send( 'Eine Newsletter Anmeldung ist momentan leider nicht möglich. Aufgetretener Fehler: ' + errorMsg ) 
        })
})




