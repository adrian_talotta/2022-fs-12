// Server Initialisierung
const express = require('express')
const { validateFormData, trimFormData, saveSubscription, saveSubscriptionCallbackAPI } = require('./helpers/form-data')

const PORT = process.env.PORT || 5000

const app = express()
app.listen(PORT, () => console.log('Server läuft auf Port', PORT))

/////////////////////////////
// Middleware Funktion
/////////////////////////////

app.use(express.static('./www'))
app.use(express.urlencoded({ extended: false }))


// Endpoints / Routes

app.post('/newsletter', async (req, res) => {
    console.log('Neue Newsletter Anmeldung ')

    trimFormData(req.body)

    // Promise API
    // if( validateFormData(req.body) ){
    //     try{
    //         // Asynchroner Lese- und Schreibvorgang wird aufgerufen
    //         await saveSubscription(req.body)
    //         res.send('Newsletter-Anmeldung war erfolgreich')
    //     } catch(err){
    //         res.send('Eine Newsletter Anmeldung ist momentan leider nicht möglich. Aufgetretener Fehler')
    //     }
    // } else {
    //     res.send( 'Newsletter-Anmeldung gescheitert. Es ist ein Problem mit den Formulardaten aufgetereten.' )
    // }

    // Callback API
    let err = validateFormData(req.body)
    if( ! err ){
            // Asynchroner Lese- und Schreibvorgang wird aufgerufen
            saveSubscriptionCallbackAPI(req.body, err => {
                const msg = 
                    err ? 
                    'Eine Newsletter Anmeldung ist momentan leider nicht möglich. Aufgetretener Fehler' 
                    :
                    'Newsletter-Anmeldung war erfolgreich'

                    res.send(msg)
            })

    } else {
        res.send( err.message, 'Name des Felds:', err.name )
    }
})