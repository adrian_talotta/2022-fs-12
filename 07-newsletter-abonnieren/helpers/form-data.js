const { readFile, writeFile } = require('fs/promises')

const PATH_JSON_NEWSLETTER = 'data/newsletter.json'
const REGEX_MAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

/**
 * Validates a string
 * @param {string} str the string to examinate
 * @param {number} min min amount of chars
 * @param {number} max miax amount of chars
 * @returns {boolean} true, if the string is valid
 */
const validateString = (str, min=2, max=30) => str && str.length >= min && str.length <= max
const validateEmail = email => ( email && REGEX_MAIL.test(email) )

module.exports.validateFormData = paramsObj => {
    // API: vorname 2-30 Zeichen (white spaces am Anfang und Ende des Strings werden nicht mitgezeählt)
    // API: vorname 2-50 Zeichen (white spaces am Anfang und Ende des Strings werden nicht mitgezeählt)
    // if(
    //     validateString(paramsObj.vorname) &&
    //     validateString(paramsObj.nachname, 2, 50) &&
    //     validateEmail(paramsObj.email)
    // ){
    //     console.log('Validierung bestanden')
    //     return true
    // } else {
    //     console.log('Validierung NICHT bestanden')
    //     return false
    // }

    let errFieldName = null
    if( ! validateString(paramsObj.vorname) )           errFieldName = 'vorname'
    else if( ! validateString(paramsObj.nachname) )     errFieldName = 'nachname'
    else if( ! validateEmail(paramsObj.email) )         errFieldName = 'email'

    if( errFieldName ) {
        return {
            name: errFieldName,
            message: `Die Angabe ${errFieldName} enthält keinen gültigen Wert.`
        }
    } else {
        return null
    }

}

module.exports.trimFormData = paramsObj => {
    for(let prop in paramsObj){
        console.log( 'Zeichenlänge Parameter ungetrimmt:', paramsObj[prop].length)
        paramsObj[prop] = paramsObj[prop].trim()
        console.log( 'Zeichenlänge Parameter getrimmt:', paramsObj[prop].length)
    }
}

module.exports.saveSubscription = formData => {
    return new Promise( async (resolve, reject) => {
        try {
            const fileData = await readFile(PATH_JSON_NEWSLETTER)
            const jsonData = JSON.parse(fileData)
            const subscriber = { vorname, nachname, email } = formData
            jsonData.subscribers.push(subscriber)
            await writeFile(PATH_JSON_NEWSLETTER, JSON.stringify(jsonData))
            resolve()
        } catch (err) {
            console.log('Error', err.name, err.message, err.code)
            reject(err)
            // throw err
        }
    } )
}

module.exports.saveSubscriptionCallbackAPI = async (formData, callback) => {
    try {
        const fileData = await readFile(PATH_JSON_NEWSLETTER)
        const jsonData = JSON.parse(fileData)
        const subscriber = { vorname, nachname, email } = formData
        jsonData.subscribers.push(subscriber)
        await writeFile(PATH_JSON_NEWSLETTER, JSON.stringify(jsonData))
        callback()
    } catch (err) {
        console.log('Error', err.name, err.message, err.code)
        callback(err)
    }
} 