// Server Initialisierung
const express = require('express')
const { validateFormData, trimFormData, saveSubscription, saveSubscriptionCallbackAPI } = require('./helpers/form-data')

const PORT = process.env.PORT || 5000

const app = express()
app.listen(PORT, () => console.log('Server läuft auf Port', PORT))

app.set( 'view engine', 'ejs' )

/////////////////////////////
// Middleware Funktion
/////////////////////////////

app.use(express.static('./www'))
app.use(express.urlencoded( { extended: false }))


// Endpoints / Routes
app.get( '/', (req, res) => res.render( 'index', { errorFieldName: '', errorMsg: '', formData: null} ))

// const {newsletterMiddleware} = require( './routers/newsletter')

app.post('/newsletter', async (req, res) => {

    trimFormData(req.body)
    const err = validateFormData(req.body)

    if( err ){
        // Validierung Formulardaten gescheitert
        // res.send( 'Newsletter-Anmeldung gescheitert.', err.message )

        res.render( 'index', {
            errorMsg: err.message,
            errorFieldName: err.fieldName, 
            formData: req.body
        })
    } else {
        // Validierung OK
        try{
            await saveSubscription(req.body)
            res.render( 'success' )
        } catch(err){
            res.render( 'index', { 
                errorMsg: `Ein Serverfehler ist aufgetereten. Art des Fehlers: ${err.name}, ${err.message}`
            })
        }
    }

})