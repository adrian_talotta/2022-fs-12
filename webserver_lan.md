# Aufruf eines lokalen Web Servers auf anderen Geräten

Die Web App am Computer über einen lokalen Web Server, z.B. via LiveServer oder NodeJS, verfügbar machen

Bei Aufruf der HTML-Seite am Computer ist die IP im Browser 127.0.0.1

Der Web Server läuft lokal am Computer, ist aber im gesamten Netzwerk verfügbar 

Dazu zählen alle Geräte, die sich denselben Router, also dieselbe Internet-Verbindung teilen

Um den lokalen Web Server von einem anderen Gerät anzusprechen, benötigen wir allerdings die netzwerk-interne IP des Computers

So finden wir diese heraus:

# Lokale IP-Adresse

Terminal Eingabe: ipconfig

Die IP Konfiguration wird ausgegeben

Die IPv4 Address ist die IP Adresse des Computers im lokalen Netzwerk

Üblicherweise beginnt sie mit 192.168.x.x  (z.B.: 192.168.1.4)

Kopieren Sie diese IP Nummer in Ihren Browser

Ihr lokaler Web Server lässt sich über diese IP bei entsprechender Port-Angabe ebenso wie über 127.0.0.1 oder localhost aufrufen

Diese IP-Adresse steht aber auch anderen Geräten im Netzwerk zur Verfügung