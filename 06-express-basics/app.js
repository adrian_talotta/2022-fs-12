/*
    Die Middleware-Architektur in Express
*/

const express = require( 'express' )
const PORT = process.env.PORT || 5000

// HTTP Server wird (nach der Klasse express.Application) erzeugt
const app = express()
app.listen( PORT, () => console.log( 'Express Server läuft auf Port', PORT ) )

///////////////////////////////////////
//   Middleware Funktionen
///////////////////////////////////////

// Die Middleware-Funktionen werden bei JEDEM einlangten HTTP-Request (auf dem entsprechenden) hintereinander ausgeführt:

app.use( (req, res, next)=>{
    console.log( 'Zweite Middleware-Funktion wurde aufgerufen, Methode des HTTP-Request', req.method )
    next()
} )

app.use( (req, res, next)=>{
    console.log( 'Erste Middleware-Funktion wurde aufgerufen, Pfad des HTTP-Request', req.path )
    next()
} )

// Endpoint für Route GET '/test'
app.get( '/test', (req, res) => {
    res.send('Test succeeded')
})