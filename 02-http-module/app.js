// Das Node Standard-Modul wird importiert
// Dieses leiefert die notwendigen Basisfunktionalitäten für einen HTTP-Webserver
const http = require('http')
const PORT = 5000

let numHomeRequest = 0

const server = http.createServer((req, res) => {

    console.log('HTTP Request ist eingelangt.')

    // Die url-Eigenschaft enthält die Request URL, jedoch ohne Protokoll-, Domain- und Port-Angabe
    // Z.B.: Vollständige URL: http://127.0.0.1:5000/test?lang=de -> request.url: /test?lang=de
    console.log('URL des Requests:', req.url)

    // Parsen der URL über die WHATWG URL Klasse (bestehende API in NodeJS)
    // Siehe: https://nodejs.org/dist/latest-v18.x/docs/api/url.html
    // Achtung: http://127.0.0.1:5000 stimmt NUR beim LOKALEN Testen (und nicht am Produktivsystem, also nach dem Hochladen auf einen Host/Provider)
    const url = new URL(req.url, 'http://127.0.0.1:5000')
    console.log('Request params:', url.searchParams)
    console.log('Request protocol:', url.protocol)

    // Die verwendete HTTP-Methode des Requests lässt sich via request.method auslesen
    console.log('Request HTTP method:', req.method)

    // Die Eigenschaft request.headers enthält alle Request Headers als Objekteigenschaften:
    console.log('Request HTTP headers:', req.headers)

    switch (url.pathname) {
        case '/':
            if (req.method === 'GET') {
                numHomeRequest++
                console.log('Der Pfad / wurde aufgerufen')

                const html = `
                <!DOCTYPE html>
                <html>
                    <head>
                        <title>Welcome home!</title>
                    <head>
                    <body>
                        <h1>Welcome home!<h1>
                        <p>Anzahl Seitenaufrufe: ${numHomeRequest}</p>
                    </body>
                </html>
            `
                res.writeHead(200, 'OK', { 'Content-Type': 'text/html' })
                res.end(html)
            } else {
                // Wrong HTTP method!
                res.writeHead(405, 'Method Not Allowed')
                res.end('Ungültige HTTP-Methode. GET erwartet.')
            }
            break
        case '/test':
            console.log('Der Pfad /test wurde aufgerufen')
            if (url.searchParams.has('name')) {
                res.end(`Hallo ${url.searchParams.get('name')}! Der Test war erfolgreich`)
            } else {
                res.writeHead(400, 'Bad Request')
                res.end('Test gescheitert, der Parameter "name" fehlt.')
            }
            break

    }
})

server.listen(PORT, () => console.log('Server läuft auf Port:', PORT))