const { readdir, stat } = require('fs/promises')

const PATH_DIR = `${__dirname}/img`

const getDirectoryContent = async () => {
    try {
        const fileNames = await readdir( PATH_DIR )
        const fileInfos = []

        // fileNames.forEach( async ( filename, i ) => {
        //     console.log( 'start forEach with', i,  filename )
        //     const filePath = `${PATH_DIR}/${filename}`
        //     const fileStats = await stat( filePath )
        //     const fileInfo = {
        //         filename: filename,
        //         lastmodified: fileStats.mtime,
        //         filesize: fileStats.size
        //     }
        //     fileInfos.push( fileInfo )
        //     console.log( 'end forEach with', i, filename )
        // } )
         
        // // Array ist in der Zeile unterhalb ist zum Zeitpunkt der Ausführung noch leer!!!
        // console.log( fileInfos )

        for( let filename of fileNames){
            console.log( 'start for with', filename )
            const filePath = `${PATH_DIR}/${filename}`
            const fileStats = await stat( filePath )
            const fileInfo = {
                filename: filename,
                lastmodified: fileStats.mtime,
                filesize: fileStats.size
            }
            fileInfos.push( fileInfo )
            console.log( 'end for with', filename )
        }

        // console.log( fileInfos )

    } catch( err ){
        console.log( 'Fehler beim Einlesen eines Ordners', PATH_DIR, err.name, err.message )
    }
}

getDirectoryContent()